************************************************
Program: example.sas
Directory: https://gitlab.oit.duke.edu/chsi-informatics/git-sandbox
Author: Tyler Schappe (tyler.schappe@duke.edu)
Investigator: Josiah Carberry
Project: An Example Analysis
Created: 5/5/2022
Purpose: An example SAS file for use with the CHSI Git sandbox repo
************************************************;


/*###############################
### Global Setup
###############################*/


*Define global variables;
%let todaysDate = %sysfunc(today(), yymmddn8.);
%let proj = P:\Pro00000001 - Psychoceramics;
%let derived = 2-DerivedData;
%let objects = 3-Output\Objects;
%let progs = 1-Programs;
%let tables = 3-Output\Tables;
%let figures = 3-Output\Figures;
